imagevis3d (3.1.0-7) unstable; urgency=medium

  * d/watch: Do not report alpha version
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1
  * Drop unneeded get-orig-source target

 -- Andreas Tille <tille@debian.org>  Tue, 09 Oct 2018 15:16:15 +0200

imagevis3d (3.1.0-6) unstable; urgency=medium

  * Moved packaging from SVN to Git
  * Standards-Version: 4.1.0 (no changes needed)
  * d/rules: s/QMAKE_LDFLAGS/QMAKE_LFLAGS/
    Closes: #876503

 -- Andreas Tille <tille@debian.org>  Sun, 24 Sep 2017 08:51:44 +0200

imagevis3d (3.1.0-5) unstable; urgency=medium

  * Fix FTBFS with Boost 1.61 (Thanks for the patch to Graham Inggs)
    Closes: #835416
  * cme fix dpkg-control
  * Remove Mathieu Malaterre from Uploaders - add myself instead just
    to make sure there is any Uploader
  * debhelper 10
  * d/watch: version=4
  * do not Build-Depend from g++
  * hardening=+all
  * DEP5
  * Fix desktop file

 -- Andreas Tille <tille@debian.org>  Thu, 17 Nov 2016 20:01:37 +0100

imagevis3d (3.1.0-4) unstable; urgency=low

  * Handle obsolete symbol in liblz4. Closes: #763246

 -- Mathieu Malaterre <malat@debian.org>  Tue, 30 Sep 2014 11:05:09 +0200

imagevis3d (3.1.0-3) unstable; urgency=low

  * Fix compilation on sparc. Closes: #751374

 -- Mathieu Malaterre <malat@debian.org>  Mon, 16 Jun 2014 12:34:10 +0200

imagevis3d (3.1.0-2) unstable; urgency=low

  * Make sure to use recent lz4. Closes: #751227
  * Really remove all references to 3rdParty
  * Use system installed lzma

 -- Mathieu Malaterre <malat@debian.org>  Wed, 11 Jun 2014 12:30:54 +0200

imagevis3d (3.1.0-1) unstable; urgency=low

  * New upstream
  * Update debian patches (lua mostly)
  * Add new deps (lz4 + lzma)

 -- Mathieu Malaterre <malat@debian.org>  Fri, 06 Jun 2014 14:14:23 +0200

imagevis3d (3.0.0-3) unstable; urgency=low

  * Use internal JPEG implementation. Closes: #688614
  * Bump Std-Vers to 3.9.5, no changes needed

 -- Mathieu Malaterre <malat@debian.org>  Tue, 31 Dec 2013 10:05:40 +0100

imagevis3d (3.0.0-2) unstable; urgency=low

  * fix kfreebsd compilation. Closes: #697015
  * fix compilation with missing jpeg symbol. Closes: #713653
  * Fix openmp compilation. Closes: #72193
  * Fix typos reported by lintian
  * Requires a g++ compiler post 4.6 release. Closes: #693143

 -- Mathieu Malaterre <malat@debian.org>  Thu, 05 Sep 2013 17:01:08 +0200

imagevis3d (3.0.0-1) unstable; urgency=low

  * New upstream: 3.0.0
    - New Grid Leaper renderer
    - Added limited support for MRC files
  * Refreshed systemlua.patch

 -- Mathieu Malaterre <malat@debian.org>  Sun, 28 Oct 2012 10:33:10 +0100

imagevis3d (2.1.1-1) unstable; urgency=low

  * New upstream: 2.1.1
    - Added support for GeomView files.
   Remove patches, applied upstream:
    - missingsharedlibs.patch 
    - tuvok_details.patch
    - maxpath.patch
    - overflow.patch
    - warning.patch
    - manual.patch
    - gcc47.patch

 -- Mathieu Malaterre <malat@debian.org>  Thu, 20 Sep 2012 16:08:41 +0200

imagevis3d (2.0.1-5) unstable; urgency=low

  * Use my @d.o alias
  * Fix compilation with gcc 4.7. Closes: #667208 
  * Fix potential issue with libtiff5
  * Use hardening flags for compilation
  * Bump Std-Vers to 3.9.3, no changes needed

 -- Mathieu Malaterre <malat@debian.org>  Fri, 04 May 2012 15:59:38 +0200

imagevis3d (2.0.1-4) unstable; urgency=low

  * Fix compilation errors on hurd/kFreeBSD.

 -- Mathieu Malaterre <mathieu.malaterre@gmail.com>  Tue, 20 Dec 2011 14:00:38 +0100

imagevis3d (2.0.1-3) unstable; urgency=low

  * Fix compilation errors on kFreeBSD.

 -- Mathieu Malaterre <mathieu.malaterre@gmail.com>  Thu, 15 Dec 2011 09:38:21 +0100

imagevis3d (2.0.1-2) unstable; urgency=low

  * Fix compilation errors.
  * Add more patches from upstream to fix warnings.

 -- Mathieu Malaterre <mathieu.malaterre@gmail.com>  Tue, 13 Dec 2011 18:53:44 +0100

imagevis3d (2.0.1-1) unstable; urgency=low

  * Initial release (Closes: #651470)

 -- Mathieu Malaterre <mathieu.malaterre@gmail.com>  Fri, 09 Dec 2011 12:04:51 +0100
